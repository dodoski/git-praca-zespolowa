package pl.edu.pwsztar;

public class Numbers {
    Double x;
    Double y;

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Numbers(Double x, Double y) {
        this.x = x;
        this.y = y;
    }
}
